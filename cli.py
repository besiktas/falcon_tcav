import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--model-type", type=str, default="base", help="select which model type to use/where are the models located")
parser.add_argument("--epochs", type=int, default=1, help="number of training epochs to use")

args = parser.parse_args()