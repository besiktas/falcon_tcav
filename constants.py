# string stuff
BAD_TIMESTAMP_LABEL = " Timestamp"
TIMESTEP_COL = "timestamp"
TARGETCOL = "Normal/Attack"

# numeric values
TIMESERIES_LENGTH = 200
BATCH_SIZE = 128