import argparse
import json
from pathlib import Path
import sys
from typing import List

import pandas as pd
import numpy as np
import tensorflow as tf

from cli import args
from constants import TARGETCOL, TIMESERIES_LENGTH
from log import get_logger
from train_nn import train_model
from tcav import TCAV
from utils import get_attack_info_df, create_concept

logger = get_logger("root")


def main(attack_nums: List[int] = [10, 11], model_type: str = "base", model_df_dir: Path = Path(__file__).parent):

    model_path = model_df_dir / f"models/{model_type}"
    df_path = model_df_dir / "data/attack_cleaned.csv"

    # data needs to already be scaled/cleaned
    if not model_path.is_dir() or not df_path.is_file():
        raise Exception("model is not dir or df is not file")

    model = tf.keras.models.load_model(model_path)
    df = pd.read_csv(df_path)
    df = df.set_index(pd.DatetimeIndex(df["timestamp"]))
    df.drop(["timestamp"], inplace=True, axis=1)

    attack_info_df = get_attack_info_df(pdf_path=model_df_dir / "docs/List_of_attacks_Final.pdf")

    concept, counterexamples = create_concept(df, attack_info_df, [10, 11], withheld_attacks=[21])
    concept21, _ = create_concept(df, attack_info_df, [21])

    concepts21_gen = tf.keras.preprocessing.sequence.TimeseriesGenerator(
        concept21.drop(TARGETCOL, axis=1).values,
        concept21[TARGETCOL].values,
        length=TIMESERIES_LENGTH,
        batch_size=1,
        shuffle=False,
    )

    concepts_gen = tf.keras.preprocessing.sequence.TimeseriesGenerator(
        concept.drop(TARGETCOL, axis=1).values,
        concept[TARGETCOL].values,
        length=TIMESERIES_LENGTH,
        batch_size=1,
        shuffle=False,
    )

    # use stride to balance the number of samples somehow?
    counterexamples_gen = tf.keras.preprocessing.sequence.TimeseriesGenerator(
        counterexamples.drop(TARGETCOL, axis=1).values,
        counterexamples[TARGETCOL].values,
        length=TIMESERIES_LENGTH,
        batch_size=1,
        stride=round(len(counterexamples) / len(concept)),
        shuffle=False,
    )

    NUM_BATCHES_BEFORE = len(concepts_gen)
    NUM_BATCHES_BEFORE = 10
    before_concept = df[df.index < concept.index[0]]

    before_concept_filtered = before_concept.iloc[
        before_concept.shape[0] - TIMESERIES_LENGTH - NUM_BATCHES_BEFORE - 1 : before_concept.shape[0] - 1
    ]

    before_concept_gen = tf.keras.preprocessing.sequence.TimeseriesGenerator(
        before_concept_filtered.drop(TARGETCOL, axis=1).values,
        before_concept_filtered[TARGETCOL].values,
        length=TIMESERIES_LENGTH,
        batch_size=1,
        shuffle=False,
    )

    # turning into arrays since easier to understand
    before_concept_x = []
    before_concept_y = []
    for x, y in before_concept_gen:
        before_concept_x.append(x)
        before_concept_y.append(y)

    concepts_x = []
    concepts_y = []
    for x, y in concepts_gen:
        concepts_x.append(x)
        concepts_y.append(y)

    concepts_x = np.array(concepts_x).squeeze()
    concepts_y = np.array(concepts_y).squeeze()

    counterexamples_x = []
    counterexamples_y = []
    for x, y in counterexamples_gen:
        counterexamples_x.append(x)
        counterexamples_y.append(y)

    counterexamples_x = np.array(counterexamples_x).squeeze()
    counterexamples_y = np.array(counterexamples_y).squeeze()

    tcav = TCAV(model)

    layer_n = 7
    tcav.use_bottleneck(layer_n)

    # for layer_n in range(1, len(tcav.model.layers) - 1):

    tcav.train_cav(concepts_x, counterexamples_x)

    def scores_for_generator(xy_gen, breakearly=-1):
        idx = 0
        # cant use enumerate on this generator
        for x, y in xy_gen:
            sensitivity_score = tcav._calculate_sensitivty(x, y)
            logger.info(f"score: {sensitivity_score.squeeze()}")
            idx += 1

            if (breakearly > 0) and (idx > breakearly):
                break

    logger.info(f"===BEFORE ATTACK===")
    scores_for_generator(before_concept_gen)

    logger.info(f"===ATTACK===")
    scores_for_generator(concepts_gen)

    logger.info(f"===DIFFERENT ATTACK===")
    scores_for_generator(concepts21_gen)


if __name__ == "__main__":

    main(model_type=args.model_type)
    # run_all_models()