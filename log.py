from pathlib import Path
import logging
from typing import Any, Callable, Union

import wandb

names = set()


def add_callback():
    pass


def add_wandb():
    pass


def add_filesaver(filename: Union[str, Path]):
    wandb.save(filename)


def _setup_custom_logger(name: str, extras=[]) -> logging.Logger:
    root_logger = logging.getLogger()
    root_logger.handlers.clear()

    formatter = logging.Formatter(fmt="%(asctime)s - %(levelname)s - %(module)s - %(message)s")

    names.add(name)

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)

    for func in extras:
        setattr(logger, str(func), func)

    return logger


def get_logger(name: str) -> logging.Logger:
    if name in names:
        return logging.getLogger(name)
    else:
        return _setup_custom_logger(name)