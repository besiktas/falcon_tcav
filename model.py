import tensorflow as tf
from tensorflow.keras.models import Model
from tensorflow.keras.layers import (
    Dense,
    Dropout,
    BatchNormalization,
    Bidirectional,
    concatenate,
    Conv1D,
    ConvLSTM2D,
    Flatten,
    GlobalMaxPooling1D,
    Input,
    Lambda,
    LSTM,
    MaxPooling1D,
    Reshape,
    TimeDistributed,
)


def make_model(*args, **kwargs):
    x_input = kwargs["x_input"]

    x_input = Input(
        shape=(
            x_input.shape[1],
            x_input.shape[2],
        ),
        name="window_input",
    )

    x = Dense(100)(x_input)
    x = Flatten()(x)
    output = Dense(1, activation="sigmoid")(x)

    model = Model(inputs=x_input, outputs=output, name="base_model")
    return model


def make_model_adv(*args, **kwargs):
    x_input = kwargs["x_input"]
    filter_size = kwargs.get("filter_size", 2)
    kernel_size = kwargs.get("kernel_size", 4)
    dilation_rate = kwargs.get("dilation_rate", 2)

    x = Input(
        shape=(
            x_input.shape[1],
            x_input.shape[2],
        ),
        name="window_input",
    )

    cnn_out = Dense(100)(x)
    cnn_out = Conv1D(filters=filter_size, kernel_size=4, activation="relu", padding="causal", dilation_rate=dilation_rate)(
        cnn_out
    )
    cnn_out = MaxPooling1D()(cnn_out)
    cnn_out = Conv1D(
        filters=filter_size * 2, kernel_size=4, activation="relu", padding="causal", dilation_rate=dilation_rate ** 2
    )(cnn_out)
    cnn_out = MaxPooling1D()(cnn_out)
    cnn_out = Conv1D(
        filters=filter_size * 3, kernel_size=4, activation="relu", padding="causal", dilation_rate=dilation_rate ** 3
    )(cnn_out)
    cnn_out = MaxPooling1D()(cnn_out)
    cnn_out = Conv1D(
        filters=filter_size * 4, kernel_size=4, activation="relu", padding="causal", dilation_rate=dilation_rate ** 4
    )(cnn_out)
    y = GlobalMaxPooling1D()(cnn_out)

    # # Non time-window input

    # y = LSTM(64, return_sequences=True)(y)
    y = Dropout(0.2)(y)
    y = Dense(64, activation="sigmoid")(y)
    output = Dense(1, activation="sigmoid", name="main_output")(y)

    model = Model(inputs=x, outputs=output, name="cnn_lstm_model")

    return model


def make_model_cnn(*args, **kwargs):
    """https://stackoverflow.com/questions/46397258/how-to-merge-sequential-models-in-keras-2-0

    Args:
        x_input ([type]): [description]
        filter_size ([type]): [description]
    """
    x_input = kwargs["x_input"]
    filter_size = kwargs.get("filter_size", 2)
    kernel_size = kwargs.get("kernel_size", 4)
    dilation_rate = kwargs.get("dilation_rate", 2)

    x_input = Input(
        shape=(
            x_input.shape[1],
            x_input.shape[2],
        ),
        name="window_input",
    )

    x = Dense(100)(x_input)
    x = Conv1D(filters=filter_size, kernel_size=kernel_size, activation="relu", padding="causal", dilation_rate=dilation_rate)(x)
    x = MaxPooling1D()(x)
    x = Conv1D(
        filters=filter_size * 2,
        kernel_size=kernel_size,
        activation="relu",
        padding="causal",
        dilation_rate=dilation_rate ** 2,
    )(x)
    x = MaxPooling1D()(x)
    x = Conv1D(
        filters=filter_size * 3,
        kernel_size=kernel_size,
        activation="relu",
        padding="causal",
        dilation_rate=dilation_rate ** 3,
    )(x)
    x = MaxPooling1D()(x)
    x = Conv1D(
        filters=filter_size * 4,
        kernel_size=kernel_size,
        activation="relu",
        padding="causal",
        dilation_rate=dilation_rate ** 4,
    )(x)
    x = MaxPooling1D()(x)
    x = Dropout(0.2)(x)
    x = Conv1D(filters=filter_size, kernel_size=kernel_size, activation="relu", padding="causal", dilation_rate=dilation_rate)(x)
    x = MaxPooling1D()(x)
    x = Conv1D(
        filters=filter_size * 2,
        kernel_size=kernel_size,
        activation="relu",
        padding="causal",
        dilation_rate=dilation_rate ** 2,
    )(x)
    x = MaxPooling1D()(x)
    x = Conv1D(
        filters=filter_size * 3,
        kernel_size=kernel_size,
        activation="relu",
        padding="causal",
        dilation_rate=dilation_rate ** 3,
    )(x)
    x = MaxPooling1D()(x)
    x = Conv1D(
        filters=filter_size * 4,
        kernel_size=kernel_size,
        activation="relu",
        padding="causal",
        dilation_rate=dilation_rate ** 4,
    )(x)
    x = GlobalMaxPooling1D()(x)
    x = Dropout(0.4)(x)
    x = Dense(64, activation="sigmoid")(x)
    output = Dense(1, activation="sigmoid", name="main_output")(x)
    model = Model(inputs=x_input, outputs=output, name="cnn_model")
    return model


def make_model_lstm(*args, **kwargs):

    x_input = kwargs["x_input"]
    _ = kwargs.get("filter_size", 2)

    x_input = Input(
        shape=(
            x_input.shape[1],
            x_input.shape[2],
        ),
        name="window_input",
    )

    x = LSTM(256, input_shape=(x_input.shape[-2], x_input.shape[-1]), return_sequences=True)(x_input)
    x = LSTM(128, return_sequences=True)(x)
    x = LSTM(64, return_sequences=False)(x)
    x = Dropout(0.4)(x)
    output = Dense(1)(x)

    model = Model(inputs=x_input, outputs=output, name="lstm_model")

    return model


def make_model_cnn_lstm(*args, **kwargs):
    x_input = kwargs["x_input"]

    x_input = Input(
        shape=(
            x_input.shape[1],
            x_input.shape[2],
        ),
        name="window_input",
    )
    x = LSTM(2 ** 6, return_sequences=True)(x_input)
    x = Conv1D(filters=2, kernel_size=4, activation="relu", padding="causal", dilation_rate=2)(x)
    x = Flatten()(x)
    output = Dense(1, activation="sigmoid")(x)
    model = Model(inputs=x_input, outputs=output, name="base_model")

    return model


def make_model_lstm_cnn(*args, **kwargs):
    x_input = kwargs["x_input"]
    filter_size = kwargs.get("filter_size", 40)
    kernel_size = kwargs.get("kernel_size", 4)
    dilation_rate = kwargs.get("dilation_rate", 2)

    model = tf.keras.Sequential(
        [
            LSTM(2 ** 8, input_shape=(x_input.shape[-2], x_input.shape[-1]), return_sequences=True),
            LSTM(2 ** 7, return_sequences=True),
            LSTM(2 ** 6, return_sequences=True),
            Dropout(0.4),
            Conv1D(
                filters=filter_size, kernel_size=kernel_size, activation="relu", padding="causal", dilation_rate=dilation_rate
            ),
            MaxPooling1D(),
            Conv1D(
                filters=filter_size * 2,
                kernel_size=kernel_size,
                activation="relu",
                padding="causal",
                dilation_rate=dilation_rate ** 2,
            ),
            MaxPooling1D(),
            Conv1D(
                filters=filter_size * 3,
                kernel_size=kernel_size,
                activation="relu",
                padding="causal",
                dilation_rate=dilation_rate ** 3,
            ),
            MaxPooling1D(),
            Conv1D(
                filters=filter_size * 4,
                kernel_size=kernel_size,
                activation="relu",
                padding="causal",
                dilation_rate=dilation_rate ** 4,
            ),
            GlobalMaxPooling1D(),
            Dropout(0.2),
            # Dense(64, activation="sigmoid"),
            Dense(1, activation="sigmoid"),
        ]
    )
    return model


MODEL_PICKER = {
    "base": make_model,
    "adv": make_model_adv,
    "cnn": make_model_cnn,
    "lstm": make_model_lstm,
    "lstm_cnn": make_model_lstm_cnn,
    "cnn_lstm": make_model_cnn_lstm,
}