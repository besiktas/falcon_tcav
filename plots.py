import json
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
import wandb

api = wandb.Api()
runs = api.runs("graham/falcon_tcav")

summary_list = []
config_list = []
name_list = []


MODEL_TYPES = ["base", "adv", "cnn", "lstm", "lstm_cnn", "cnn_lstm"]


MODEL_TYPES = ["base", "adv", "cnn", "lstm", "lstm_cnn", "cnn_lstm"]
model_type = MODEL_TYPES[1]


def make_chart_sensitivity(model_type):

    run = list(filter(lambda x: model_type in x.tags and "tcav" in x.tags and x.state == "finished", runs))[0]
    run_history = run.history()
    scores = []
    for row_idx, row in run_history.iterrows():
        scores.append({"layer": row.layer_n, "score": row.sensitivity_score[0], "score_n": 0})
        scores.append({"layer": row.layer_n, "score": row.sensitivity_score[1], "score_n": 1})

    df = pd.DataFrame(scores)

    ax = sns.catplot(data=df, kind="bar", x="layer", y="score", hue="score_n")

    plt.title(model_type)

    ax.savefig(f"plots/sensitivity_by_layer_{model_type}.png")
    return ax


for idx, n in enumerate(MODEL_TYPES):
    ax = make_chart_sensitivity(model_type=MODEL_TYPES[idx])