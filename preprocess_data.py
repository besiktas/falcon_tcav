from pathlib import Path

import numpy as np
import pandas as pd
import pywt
from sklearn.preprocessing import MinMaxScaler

from constants import BAD_TIMESTAMP_LABEL, TIMESTEP_COL, TARGETCOL, TIMESERIES_LENGTH, BATCH_SIZE


# def get_train_validation_data(filepath: str = "data/attack_cleaned.csv"):


def add_wavelet(data, wavelet_=pywt.Wavelet("db1")):
    cA, cD = pywt.dwt(data, wavelet=wavelet_, mode="periodic", axis=1)
    data = np.concatenate((data, cA, cD), axis=1)

    return data


def use_min_max_scaler(df, min_max_scaler):
    cols_dont_use = [TARGETCOL]
    for col in df.columns.to_list():
        if col not in cols_dont_use:
            df[col] = min_max_scaler.fit_transform(np.array(df[col].values, dtype=np.float32).reshape(-1, 1))
    return df


def load_data(filename: str = "attack.xlsx", using_colab: bool = False) -> pd.DataFrame:
    if using_colab:
        BASE_DATA_DIR = "/content/drive/My Drive/Colab Notebooks/FALCON"
    else:
        BASE_DATA_DIR = Path().cwd().resolve()

    df = pd.read_excel(BASE_DATA_DIR / filename)

    return df


def transform_data(df, add_wavelet=True):
    """given a dataframe do any transforms here so we can feed it to model

    not splitting into x,y b/c makes it hard to check concepts and easier to just
    drop TARGETCOL when needed

    also sets timestamp as a dateindex so we can filter by it later
    """

    if BAD_TIMESTAMP_LABEL in df.columns:
        df[TIMESTEP_COL] = pd.to_datetime(df[BAD_TIMESTAMP_LABEL])

    df = df.set_index(pd.DatetimeIndex(df[TIMESTEP_COL]))

    # lastly, remove this column from actual columns
    for col in [BAD_TIMESTAMP_LABEL, TIMESTEP_COL]:
        if col in df.columns:
            df = df.drop([col], axis=1)

    return df


def data_to_csv():
    # dataset = load_data(filename="normal.xlsx")
    full_attack = load_data(filename="data/attack.xlsx")
    full_attack = full_attack.drop(full_attack.index[230310:264238]).reset_index(drop=True)
    # original_y = full_attack[TARGETCOL].values

    full_attack[TARGETCOL] = full_attack[TARGETCOL].apply(lambda x: 0 if "N" in x else 1)

    full_attack = transform_data(full_attack)

    MIN_MAX_SCALER = MinMaxScaler(feature_range=(0, 1))
    full_attack = use_min_max_scaler(full_attack, MIN_MAX_SCALER)

    return full_attack


if __name__ == "__main__":
    df = data_to_csv()

    df.to_csv("data/attack_cleaned.csv")