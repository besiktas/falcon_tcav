notes:

https://github.com/tensorflow/tcav/issues/50

## gradients related pages/tutorials

- https://keras.io/examples/vision/integrated_gradients/
- https://stackoverflow.com/questions/60083156/getting-gradients-of-each-layer-in-keras-2
- https://www.tensorflow.org/guide/eager#computing_gradients

### gradient tape related

- https://github.com/tensorflow/tensorflow/issues/33135

## implementations:

- pytorch
  - https://github.com/rakhimovv/tcav
- tensorflow
  - https://github.com/fursovia/tcav_nlp/blob/master/model/model_fn.py
  - https://github.com/tensorflow/tcav/blob/master/tcav/model.py
  - https://github.com/tensorflow/tcav/blob/master/tcav/model.py
  - https://github.com/tensorflow/tcav/blob/master/tcav/tcav.py
  - https://github.com/pnxenopoulos/cav-keras/blob/master/cav/tcav.py
  - https://github.com/pnxenopoulos/cav-keras/blob/master/docs/examples/cifar_example.ipynb

# usage

1. run `python preprocess_data.py`

   - this makes a new csv with the data and does minmaxscaling and whatever else.

   - seemed like csv loads way quicker for testing later parts

2. run `python train_nn.py`
   - this trains the neural network architecture from model.py and saves to a folder
3. run `python run_tcav.py`
   - this is more of a script and does all the tcav stuff. if you want to do everything in one file, this would be ideally where you clean the data, train the model and then calculate tcav. you can import functions from the other files.

## using wandb (weights and biases)

im using the platform/framework wandb to log stuff
to make it not log something use something like `env WANDB_MODE=dryrun python3.8 train_nn.py --epochs 1` (im using fish as my shell for bash just remove the env)

## using docker on iapetus with gitlab-runner

```
docker run --privileged --name gitlab-runner --restart always \
   -v /srv/gitlab-runner/config:/etc/gitlab-runner \
   -v /var/run/docker.sock:/var/run/docker.sock  \
   -v /home/graham/build:/buildshare \
   gitlab/gitlab-runner:latest
```

```
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
```

```
docker run -it \
      -v /srv/gitlab-runner/config:/etc/gitlab-runner \
      -v /var/run/docker.sock:/var/run/docker.sock \
      ubuntu:18.04 bash
```
