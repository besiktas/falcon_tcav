#! /bin/bash

# python -m pip install -r requirements.txt
# MODEL_TYPES=("base" "cnn" )
MODEL_TYPES=("base" "adv" "cnn" "lstm" "lstm_cnn" "cnn_lstm")

if [[ -z "${TRAINING_EPOCHS}" ]]; then
    TRAINING_EPOCHS=1
fi

echo using TRAINING_EPOCHS: ${TRAINING_EPOCHS}

train_all() {

    for model in ${MODEL_TYPES[@]}; do
        python train_nn.py --model-type $model --epochs $TRAINING_EPOCHS
    done

}

tcav_all() {
    for model in ${MODEL_TYPES[@]}; do
        python run_tcav.py --model-type $model
    done
}

train_all
tcav_all
