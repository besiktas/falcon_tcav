import argparse
from collections import Counter

import pandas as pd
import numpy as np
import tensorflow as tf
from sklearn.metrics import f1_score

import wandb
from wandb.keras import WandbCallback

from cli import args
from constants import BAD_TIMESTAMP_LABEL, TARGETCOL, TIMESERIES_LENGTH, TIMESTEP_COL, BATCH_SIZE
from log import get_logger
from model import MODEL_PICKER
from preprocess_data import transform_data
from utils import Artifact, DatasetHelper


logger = get_logger("root")
# need this to use calculate the tcav/sensitivity stuff the old way
tf.compat.v1.disable_eager_execution()


def get_class_weights(y, smooth_factor=0):
    counter = Counter(y)
    if smooth_factor > 0:
        p = max(counter.values()) * smooth_factor
        for k in counter.keys():
            counter[k] += p
    majority = max(counter.values())
    return {cls: float(majority / count) for cls, count in counter.items()}


def train_model(num_epochs: int = 1, full_attack_path: str = "data/attack_cleaned.csv", model_type: str = "base"):

    window_size = 200
    nb_filter = 5

    logger.debug(f"Tensorflow Version: {tf.version.VERSION}")
    logger.debug(f"Num GPUs Available: {len(tf.config.experimental.list_physical_devices('GPU'))}")

    dataset = DatasetHelper(data=pd.read_csv(full_attack_path), targetcol=TARGETCOL, split_amount=0.7)

    # breakpoint()
    train_data_gen = tf.keras.preprocessing.sequence.TimeseriesGenerator(
        dataset.x,
        dataset.y,
        length=TIMESERIES_LENGTH,
        end_index=dataset.split_index,
        batch_size=BATCH_SIZE,
        shuffle=True,
    )

    validation_data_gen = tf.keras.preprocessing.sequence.TimeseriesGenerator(
        dataset.x,
        dataset.y,
        length=TIMESERIES_LENGTH,
        start_index=dataset.split_index,
        batch_size=BATCH_SIZE,
        shuffle=True,
    )

    x_train_batch, y_train_batch = train_data_gen[0]

    logger.info(f" x shape: {x_train_batch.shape} \n y shape: {y_train_batch.shape}")
    logger.info(f"len of train_data_gen: {len(train_data_gen)} and len of validation_data_gen: {len(validation_data_gen)}")
    logger.info(f"size of x_input: {x_train_batch.shape} and filter_size={nb_filter}")

    model = MODEL_PICKER[model_type](**{"x_input": x_train_batch, "filter_size": nb_filter})

    METRICS = [
        tf.keras.metrics.TruePositives(name="tp"),
        tf.keras.metrics.FalsePositives(name="fp"),
        tf.keras.metrics.TrueNegatives(name="tn"),
        tf.keras.metrics.FalseNegatives(name="fn"),
        tf.keras.metrics.BinaryAccuracy(name="acc"),
        tf.keras.metrics.AUC(name="auc"),
        tf.keras.metrics.Precision(name="precision"),
        tf.keras.metrics.Recall(name="recall"),
    ]

    model.compile(loss=["binary_crossentropy"], optimizer="adam", metrics=METRICS)

    # train model
    class_weights = get_class_weights(dataset.y, smooth_factor=0)

    # counts = np.bincount(dataset.y[: dataset.split_index])
    # class_weights = {0: 1.0 / counts[0], 1: 1.0 / counts[1]}

    model.fit(
        train_data_gen,
        validation_data=validation_data_gen,
        epochs=num_epochs,
        class_weight=class_weights,
        callbacks=[WandbCallback(), tf.keras.callbacks.EarlyStopping(monitor="loss", patience=3)],
    )

    results = model.evaluate(validation_data_gen)
    logger.info(f"for {model.metrics_names}\ngot results: {results}")

    # y_preds = model.predict(validation_data_gen)
    # y_preds[y_preds < 0.5] = 0
    # y_preds[y_preds > 0.5] = 1

    # y_true = validation_data_gen.targets[validation_data_gen.start_index : validation_data_gen.end_index + 1]
    # y_preds = y_preds.flatten()

    # f1 = f1_scorey_true, y_preds)

    return model, dataset


if __name__ == "__main__":
    run = wandb.init(project="falcon_tcav", job_type="train", tags=["training", f"{args.model_type}"])

    model, _ = train_model(num_epochs=args.epochs, model_type=args.model_type)

    model_path = f"models/{args.model_type}"
    model.save(model_path)

    # WANDB STUFF BELOW
    artifacts = [
        Artifact.use_file(filepath="data/attack_cleaned.csv", name="attack-cleaned", type="dataset"),
        Artifact.use_dir(filepath=model_path, name=f"model-{args.model_type}", type="model"),
    ]

    for a in artifacts:
        a.log_artifact(run)

    # no idea what the difference between an artifact and this is
    wandb.save(model_path)
