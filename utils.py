from dataclasses import dataclass
import datetime
import math
import time
import re

import tabula
import pandas as pd
import numpy as np
import tensorflow as tf
import wandb

from constants import TARGETCOL
from preprocess_data import transform_data


@dataclass
class Artifact:
    name: str
    type: str

    def __post_init__(self):
        self._artifact = wandb.Artifact(self.name, type=self.type)

    @classmethod
    def use_file(cls, filepath, name, type):
        instance = cls(name, type)
        instance._artifact.add_file(filepath)
        return instance

    @classmethod
    def use_dir(cls, filepath, name, type):
        instance = cls(name, type)
        instance._artifact.add_dir(filepath)
        return instance

    def log_artifact(self, run):
        run.log_artifact(self._artifact)


class DatasetHelper:
    def __init__(self, data: pd.DataFrame, targetcol: str = TARGETCOL, split_amount: float = 0.7):
        self.data = transform_data(data)
        self.targetcol = targetcol
        self.split_amount = split_amount

    def __len__(self):
        return len(self.data)

    @property
    def split_index(self):
        return round(len(self.data) * self.split_amount)

    @property
    def x(self):
        return self.data.drop(self.targetcol, axis=1).values[1:, :]

    @property
    def y(self):
        return self.data[self.targetcol].values[:-1]

    # def __getitem__(self, idx):
    #     pass


def recall_m(y_true, y_pred):
    true_positives = tf.keras.backend.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = tf.keras.backend.sum(tf.keras.backend.round(tf.keras.backend.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + tf.keras.backend.epsilon())
    return recall


def precision_m(y_true, y_pred):
    true_positives = tf.keras.backend.sum(tf.keras.backend.round(tf.keras.backend.clip(y_true * y_pred, 0, 1)))
    predicted_positives = tf.keras.backend.sum(tf.keras.backend.round(tf.keras.backend.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + tf.keras.backend.epsilon())
    return precision


def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + tf.keras.backend.epsilon()))


def use_min_max_scaler(df, min_max_scaler):
    cols_dont_use = [TARGETCOL]
    for col in df.columns.to_list():
        if col not in cols_dont_use:
            df[col] = min_max_scaler.fit_transform(df[col])

    return df


def read_attack_info_pdf(pdf_path):
    dfs = tabula.read_pdf(pdf_path)
    return dfs


def transform_attack_info_df(df):
    split_cols = re.findall("[A-Z][^A-Z]*", df.columns[0])

    # Start index of labels
    cols = [0, 1, 3, 5, 7, 9, 10, 12, 14, 17]

    # old_column_names = df.columns
    df.columns = ["".join(split_cols[cols[idx] : cols[idx + 1]]) for idx in range(0, len(cols) - 1)]

    df["starttime"] = pd.to_datetime(df["Start Time"])
    # temp row we change time
    df["endtime_tmp"] = df[df["End Time"].notna()]["End Time"].apply(pd.Timestamp)
    df["endtime"] = df[df["endtime_tmp"].notna()].apply(
        lambda x: x["starttime"].replace(
            hour=x["endtime_tmp"].hour, minute=x["endtime_tmp"].minute, second=x["endtime_tmp"].second
        ),
        axis=1,
    )
    # drop temp row
    df.drop("endtime_tmp", axis=1, inplace=True)
    return df


def get_attack_info_df(pdf_path: str = "docs/List_of_attacks_Final.pdf"):
    """read attack info pdf using tabula-py or whatever

    Returns:
        pd.Dataframe: transformed dataframe
    """
    dfs = read_attack_info_pdf(pdf_path)
    return transform_attack_info_df(dfs[0])


def create_concept(df, attack_info_df, attack_nums, withheld_attacks=[]):
    """seperate a concept dataframe (index of attack info dataframe)
    and not concept dataframe

    Args:
        df (pd.Dataframe): dataframe of all the data
        attack_info_df (pd.Dataframe): dataframe of attack info
        attack_nums (List[int]): attack numbers to use
        withheld_attacks (List[int]): lists of attacks to not include in counterexamples

    Returns:
        [df, df]: concepts, counterexamples
    """

    concepts = []
    for attack_num in attack_nums:

        starttime, endtime = get_starttime_endtime(attack_info_df, attack_num)

        # create concept dataframe, merge with prev if another
        concept = df[(df.index >= starttime) & (df.index <= endtime)]
        concepts.append(concept)

    withheld_concepts = []
    for attack_num in withheld_attacks:
        starttime, endtime = get_starttime_endtime(attack_info_df, attack_num)
        withheld_concept = df[(df.index >= starttime) & (df.index <= endtime)]
        withheld_concepts.append(withheld_concept)

    concept = pd.concat(concepts)
    concept = concept[~concept.index.duplicated()]

    counterexamples = df.drop(labels=concept.index, axis=0)

    if withheld_concepts:
        withheld_concept = pd.concat(withheld_concepts)
        withheld_concept = withheld_concept[~withheld_concept.index.duplicated()]
        counterexamples = counterexamples.drop(labels=withheld_concept.index, axis=0)

    return concept, counterexamples


def get_starttime_endtime(attack_info_df, attack_num):
    attack_row_idx = attack_info_df[attack_info_df["Attack #"] == attack_num].index.values[0]
    attack_row = attack_info_df.iloc[attack_row_idx]

    starttime = attack_row.starttime
    endtime = attack_row.endtime
    return starttime, endtime
